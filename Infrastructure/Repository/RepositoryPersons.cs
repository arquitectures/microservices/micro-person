﻿using Infrastructure.Context;
using Model.Interfaces.Repository;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class RepositoryPersons : IRepositoryBase<Person>
    {

        private CampusVirtualContext _context;

        public RepositoryPersons(CampusVirtualContext context)
        {
            this._context = context;
        }

        public Person Add(Person entity)
        {
            this._context.Person.Add(entity);

            return entity;
        }

        public void Commit()
        {
            this._context.SaveChanges();
        }

        public bool Delete(Person entity)
        {
            this._context.Person.Remove(entity);

            return true;
        }

        public IList<Person> Get()
        {
            return this._context.Person.ToList();
        }

        public Person Get(int value)
        {
            return this._context.Person.Where(p => p.Id == value).FirstOrDefault();
        }

        public bool Update(Person entity)
        {
            this._context.Person.Update(entity);

            return true;
        }
    }
}
