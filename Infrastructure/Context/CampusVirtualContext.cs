﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Context
{
    public class CampusVirtualContext:DbContext
    {
        public DbSet<Person> Person { get; set; }
        public DbSet<Project> Project { get; set; }

        public CampusVirtualContext() :base()
        {
        }

        public CampusVirtualContext(DbContextOptions<CampusVirtualContext> options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured) { optionsBuilder.UseSqlServer(Program.Conn); }

            optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog = CampusDB;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PersonConfig());
            modelBuilder.ApplyConfiguration(new ProjectConfig());
        }
    }
}
