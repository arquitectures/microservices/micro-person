# micro-person

## Getting started

This project has been created as a template for a simple microservice. 

## Microservice Description
In this microservice architecture for managing people with three distinct layers: domain, application, and infrastructure, each layer plays a specific role in the system's architecture:

**Domain Layer:**

This layer contains the business logic and domain-specific rules related to managing people.
It includes entities and value objects representing domain concepts such as Persons, Employees, Customers, etc.
It may also contain interfaces and services defining allowed operations on these entities, such as creating, modifying, and deleting people.

**Application Layer:**

The application layer acts as an intermediate layer between the domain layer and the infrastructure.
It orchestrates domain operations and adapts them for use in the underlying infrastructure.
Specific use cases of the people microservice are implemented here using services and entities defined in the domain layer.
It may contain additional logic for data validation, transaction management, data transformation, etc.

**Infrastructure Layer:**

This layer provides the technological components necessary to run and deploy the people microservice.
It includes components such as databases, storage services, authentication services, messaging services, etc.
Adapters that enable communication with external services and data persistence are implemented here.
It is also responsible for configuring and managing the underlying infrastructure, such as scalability, availability, and security of the service.
In summary, the people microservice is organized into three clearly defined layers: domain, application, and infrastructure. Each layer has specific responsibilities and contributes to the effective construction and operation of the service as a whole.

## DI 

**ILogger<PersonController> _logger**: This is an example of a dependency injected in the constructor. The ILogger<T> interface is used to log logging information specific to the PersonController controller. This allows the controller to log messages in the system.

**IServiceBase<Person> _service**: Another example of a dependency injected in the constructor. This is a generic service (IServiceBase<T>) that operates on the Person entity. The exact details of this service are not visible in the provided code, but it likely provides methods for performing CRUD (Create, Read, Update, Delete) operations on Person objects.

## Controller Methods

**Get()**: This method is associated with an HTTP GET request and returns a collection of Person objects, which are obtained by invoking the Get() method of the _service service.

**Update(Person value)**: This method is associated with an HTTP PUT request and updates a specified Person object. It uses the _service service to perform the update, passing the Person object as a parameter.

**Add(Person value)**: This method is associated with an HTTP POST request and adds a new Person object to the system. It uses the _service service to add the specified Person object, passing it as a parameter.

**Delete(Person value)**: This method is associated with an HTTP DELETE request and deletes a specified Person object. It uses the _service service to perform the deletion, passing the Person object as a parameter.

In summary, the **PersonController** class uses **dependency injection** to receive instances of a logger and a generic service in its constructor, and then uses this service to perform **CRUD operations** on the Person entity through methods associated with different types of HTTP requests.
