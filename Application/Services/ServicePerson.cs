﻿using Application.Interfaces;
using Model;
using Model.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Services
{
    public class ServicePerson : IServiceBase<Person>
    {
        private IRepositoryBase<Person> _repoPerson;

        public ServicePerson(IRepositoryBase<Person> repoPerson)
        {
            this._repoPerson = repoPerson;
        }
        public IList<Person> Get()
        {
            return this._repoPerson.Get();
        }

        public Person Get(int value)
        {
            return this._repoPerson.Get(value);
        }

        public Person Add(Person value)
        {
            var response = this._repoPerson.Add(value);

            this._repoPerson.Commit();

            return response;
        }

        public bool Update(Person value)
        {
            var response = this._repoPerson.Update(value);

            this._repoPerson.Commit();

            return response;
        }

        public bool Delete(Person value)
        {

            var response = this._repoPerson.Delete(value);

            this._repoPerson.Commit();

            return response;
        }

        public void Commit()
        {
            this._repoPerson.Commit();
        }
    }
}
