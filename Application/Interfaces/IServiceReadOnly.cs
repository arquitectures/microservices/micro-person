﻿using Model.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IServiceReadOnly<TEntidad>
        : IRepositoryReadOnly<TEntidad>
    {

    }
}
