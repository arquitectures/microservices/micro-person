﻿using Model.Interfaces;
using Model.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IServiceBase<TEntity>
        : IAdd<TEntity>, IUpdate<TEntity>, IDelete<TEntity>, IGet<TEntity>
    {

    }
}
