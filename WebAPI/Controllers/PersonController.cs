using Application.Interfaces;
using Application.Services;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.Interfaces.Repository;
using System;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly ILogger<PersonController> _logger;
        private readonly IServiceBase<Person> _service;

        public PersonController(ILogger<PersonController> logger, IServiceBase<Person> service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet(Name = "Get")]
        public IEnumerable<Person> Get()
        {
            return this._service.Get();
        }

        [HttpPut(Name = "Update")]
        public bool Update(Person value)
        {
            return this._service.Update(value);
        }

        [HttpPost(Name = "Add")]
        public Person Add(Person value)
        {
            return this._service.Add(value);
        }

        [HttpDelete(Name = "Delete")]
        public bool Delete(Person value)
        {
            return this._service.Delete(value);
        }
    }
}
