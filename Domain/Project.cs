﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Project
    {
        public int Id { get; set; }
        public string ProjectCode { get; set; }
        public string ProjectNameEn { get; set; }
        public string ProjectNameEs { get; set; }
        public string ProjectNameFr { get; set; }
        public DateTime? ProjectCreationDate { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
        public DateTime? ProjectCompletedDate { get; set; }
        public string ProjectTypeCode { get; set; }
        public string ProjectTypeNameEn { get; set; }
        public string ProjectTypeNameEs { get; set; }
        public string ProjectTypeNameFr { get; set; }
        public string StatusCode { get; set; }
        public string StatusNameEn { get; set; }
        public string StatusNameEs { get; set; }
        public string StatusNameFr { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyNameEn { get; set; }
        public string CompanyNameEs { get; set; }
        public string CompanyNameFr { get; set; }
        public string EntityCode { get; set; }
        public string EntityNameEn { get; set; }
        public string EntityNameEs { get; set; }
        public string EntityNameFr { get; set; }
        public string MissionCode { get; set; }
        public string MissionNameEn { get; set; }
        public string MissionNameEs { get; set; }
        public string MissionNameFr { get; set; }
    }
}
