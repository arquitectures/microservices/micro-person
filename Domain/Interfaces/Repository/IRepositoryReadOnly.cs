﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Interfaces.Repository
{
    public interface IRepositoryReadOnly<TEntity> : IGet<TEntity>
    {
    }
}
